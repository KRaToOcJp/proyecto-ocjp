/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author FERNANDO
 */
public class ValidarPlacasPichincha extends Exception {
    
    private String Error;
     
    public ValidarPlacasPichincha(String codigoError){
        super();
        this.Error=codigoError;
    }
     
    @Override
    public String getMessage(){
         
        String mensaje="";
         
        switch(Error){
            case "P":
                mensaje="Error, la placa no empieza con P";
                break;
            case "p":
                mensaje="Error, la placa no existe";
                break;
            case "G":
                mensaje="Error, la placa no es de Quito";
                break;
        }
         
        return mensaje;
         
    }
    
    
}
