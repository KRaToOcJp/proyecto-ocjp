/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

/**
 *
 * @author julio_000
 */
public class Propietario {
    public String cedula;
    public String Nombre;
    public int edad;
    public int clave_vehiculo;

    public Propietario() {
    }

    public Propietario(String cedula, String Nombre, int edad, int clave_vehiculo) {
        this.cedula = cedula;
        this.Nombre = Nombre;
        this.edad = edad;
        this.clave_vehiculo = clave_vehiculo;
    }

    @Override
    public String toString() {
        return "Propietario{" + "cedula=" + cedula + ", Nombre=" + Nombre + ", edad=" + edad + ", clave_vehiculo=" + clave_vehiculo + '}';
    }
    public void tienevechiculo(vehiculosTerrestres vehiculo)
    {
    System.out.println("El propietario "+this.toString()+ "tiene un "+vehiculo.toString());
    }
}
