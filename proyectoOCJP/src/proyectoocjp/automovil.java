/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

import static proyectoocjp.autobus.adicional;

/**
 *
 * @author FERNANDO
 */
public abstract class automovil extends vehiculosTerrestres{ //se realizo la clase abtract automovil

     //definicion de atributos 
    public String placa;
    public String estado;
    public static final double precio=30000;// atributo final
    
    //constructores 
    public abstract String automovil();
   

    public automovil(String placa, String estado) {
        this.placa = placa;
        this.estado = estado;
        
        System.out.println("El automovil tiene una placa :  "+placa+ "    Estado  :   "+estado+"    Adicional: "+adicional);
    }
    
    
    public abstract boolean pasorevision();
    
}
