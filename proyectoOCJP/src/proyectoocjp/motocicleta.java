/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

import static proyectoocjp.autobus.adicional;

/**
 *
 * @author FERNANDO
 */
public class motocicleta extends vehiculosTerrestres{
      
      //definicion de atributos 
    public String marca;
    public String estado;
    public int motor;
    public String fabricante;
    public int velocidadMaxima;
  
    public static final double precio=1000;// Uso de modificador final
    
    //Creacion de constructores 
    public motocicleta() {
    }
    
    //Sobrecarga de constructores

//HEAD
   
    

    public motocicleta(String marca, String estado) {
        this.marca = marca;
        this.estado = estado;
    }
//Sobreescritura_de_metodos
    public motocicleta(String marca, String estado,String motor) {
        this.marca = marca;
        this.estado = estado;        
    }
     public motocicleta(String marca, String estado,int velocidadMaxima,int motor) {
        this.marca = marca;
        this.motor=motor;
        this.estado = estado;
        this.velocidadMaxima=velocidadMaxima;
        System.out.println("La moto es de marca: "+marca +" Velocidad :   "+velocidadMaxima+ "km/s  Estado:  "+ estado+"  Adicional: "+adicional+ " Motor: "+motor);
    }
     //SI SE DESEA CAMBIAR INFORMACION CON RESPECTO A EL AUTOBUS - PLACA, ESTADO
    public void cambiarDatos(String plac, String est){
        
       marca=plac;
       estado=est;
    }

    //SI SE DESEA CAMBIAR INFORMACION CON RESPECTO A la motocicleta - marca, ESTADO
    public void cambiarDatos(String plac){
        
       marca=plac; 
    }
         //Calcular el costo total a pagar por revision-- Sobreescritura de metodos
    public void calcularCostoT(vehiculosTerrestres moto)  {
       this.costoTotal=this.costoRevision*1.12;
     }

    @Override
    public String toString() {
        return "motocicleta{" + "marca=" + marca + ", estado=" + estado + ", motor=" + motor + ", fabricante=" + fabricante + ", velocidadMaxima=" + velocidadMaxima + '}';
    }
    
    
}
