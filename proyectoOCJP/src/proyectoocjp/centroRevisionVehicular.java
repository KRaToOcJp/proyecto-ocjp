/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoocjp;

import Ciudades.Quito;
import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import static proyectoocjp.autobus.adicional;

/**
 *
 * @author FERNANDO
 */
class Interna //Inner Class
{
    public void imprimir(){//metodo dentro de la clase interna
        System.out.println("Esta es la clase Interna 1");
        
    }
}
class Interna2
{
    private int x = 10;
    
//Method Local Inner Classes
    public void createLocalInnerClass()//metodo que crea la clase local interna
    {
        int y = 15;
        final int suma =x+y;
        class LocalInner//clase local dentro del metodo
        {
            public void accessOuter() //metodo dentro de la clase local 
            {
                System.out.println(x);
                //System.out.println(y); 
                System.out.println(suma);
            }
        }
        
        LocalInner li = new LocalInner();//instanciacion de la clase para poder acceder desde fuera
        li.accessOuter();//invocacion metodo de la clase
    }
    
}
abstract class Anonima{//clase anonima usando abstract
abstract void accionAnonima();

}
interface Anonima2{
void imprimir();
}
class Interna3
{
static int a=30;
static int b=20;
static int multi=a*b;
  static class Anidada{  //clase statica anidada
   void multiplicacion(){System.out.println("La multiplicacion es:"+multi);} //metodo de la clase anidada
  }  
}
public class centroRevisionVehicular implements Runnable{

    /**
     * @param args the command line arguments
     */
    motocicleta motoC;
    //Creacion de los diferentes asserts
     public void pruebaAssert(int i){
        int valorActualCapacidadMax=i;
         System.out.println("En el caso que la capacidad maxima del parqueadero sea menor a O,este valor se cambiara"+valorActualCapacidadMax);        
        assert valorActualCapacidadMax>=0 : valorActualCapacidadMax=50;
         }
     
     public void assertValidarVehRec(int i){
        int valorActualCapacidadMax=i;
         System.out.println("En el caso que el tamaño del vehiculo recibido sea menor a O,este valor se cambiara, valor actual: "+valorActualCapacidadMax);        
        assert valorActualCapacidadMax>=0 : valorActualCapacidadMax=0;
         }
     
      public void assertValidarPuestosSob(int i){
        int valorActualCapacidadMax=i;
         System.out.println("En el caso que el tamaño de puestos sobrantes en el parqueadero sea menor a O,este valor se cambiara, valor actual: "+valorActualCapacidadMax);        
        assert valorActualCapacidadMax>0 : valorActualCapacidadMax=50;
         }
      
      public void assertValidarPorcentajeLivia(int i){
        int valorActualCapacidadMax=i;
         System.out.println("En el caso que el tamaño de puestos sobrantes en el parqueadero sea menor a O,este valor se cambiara, valor actual: "+valorActualCapacidadMax);        
        assert valorActualCapacidadMax>=0 : valorActualCapacidadMax=0;
         }
      
      
      
     
    public static void main(String[] args) {
        // TODO code application logic here
        parqueadero parqueo = new parqueadero();
        Date fecha = new Date();
        Quito quito= new Quito();
        vehiculosTerrestres vh = new vehiculosTerrestres();
        final resultadoRevision rr = resultadoRevision.APROVADO;
        parqueo.imprimirLista();
        
        
        vh.setCodTurnoRevision(00003);
        
        
        System.out.println("Bienvenido al centro de Revisión Vehicular");
        System.out.println("\nDistribución del Parqueadero: ");Livianos(parqueo);
        Pesados(parqueo);
        Rechazados(parqueo);
        
        
        System.out.println("\nResultado centro de Revisión Vehicular");
        System.out.println ("Fecha: "+fecha);
        System.out.println ("Ciudad: "+vh.Ciudad+ "  Cod Provincia: "+quito.codProvincia);
        
        System.out.println ("\nResultados de la Revisión de automóviles: \n");
        System.out.println("Turno Revisión: "+vh.getCodTurnoRevision());

        automovil auto = new automovil("DSE-253","Bueno") {
        @Override
        public String automovil() {
             return "El automovil tiene una placa : "+placa+ " Estado: "+estado+" Adicional: "+adicional +"Resultado "+ rr.name();
        }
        @Override
        public boolean pasorevision() {
            return true;
        }
    };
        
        
      
         (new Thread(new centroRevisionVehicular())).start();
        (new Thread(new centroRevisionVehicular())).start();
         (new Thread(new centroRevisionVehicular())).start();
        
     
        //  salidaClientes();

    
    }
    //fin main
   
    public void run() {
        System.out.println("corrio en hilo");
        Date fecha = new Date();
       
        
        Thread ct = Thread.currentThread();
        System.err.println("Ver valor del thread"+ct);
        if (ct.getName().equals("Thread-0")) {
            
            System.err.println("Ingreso al primer hilo");
            System.out.println ("\nResultados de la Revisión de autobuses: \n");
       vehiculosTerrestres vh = new vehiculosTerrestres();
        final resultadoRevision rr = resultadoRevision.APROVADO;
       autobus autoB=new autobus("PHZ-455", "Bueno");
       System.out.println("  Resultado Revisión:"+rr.APROVADO);

       vh.setCodTurnoRevision(0004);
       System.out.println("Turno Revisión: "+vh.getCodTurnoRevision());
       autoB = new autobus("PFA-455", "Excelente");
       System.out.println("  Resultado Revisión:"+rr.RECHAZADO);
       vh.setCodTurnoRevision(0005);
       System.out.println("Turno Revisión: "+vh.getCodTurnoRevision());
       autoB = new autobus("PFA-178", "Regular");
       System.out.println("  Resultado Revisión:"+rr.APROVADO);
       vh.setCodTurnoRevision(0006);
       System.out.println("Turno Revisión: "+vh.getCodTurnoRevision());
       autoB = new autobus("PFA-963", "Bueno");
       System.out.println("  Resultado Revisión:"+rr.POSTERGADO);
       vh.setCodTurnoRevision(0007);
       System.out.println("Turno Revisión: "+vh.getCodTurnoRevision());
       autoB = new autobus("PFA-145", "Excelente");
       System.out.println("  Resultado Revisión:"+rr.APROVADO);
       System.out.println("Turno Revisión: "+"0008");
       autoB = new autobus("PFA-0125", "Regular");
       System.out.println("  Resultado Revisión:"+rr.POSTERGADO);
       System.out.println("Turno Revisión: "+"0009");
       autoB = new autobus("PFA-011", "Bueno");
       System.out.println("  Resultado Revisión:"+rr.RECHAZADO);
       motocicleta motoC= new motocicleta("HONDA", "Revision","56K");
 try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(centroRevisionVehicular.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (ct.getName().equals("Thread-1")) {
       System.err.println("segundo");
            System.out.println("Ingreso");
            System.out.println ("\nResultados de la Revisión de motocicletas: \n");
        final resultadoRevision rr = resultadoRevision.APROVADO;
        motoC = new motocicleta("SUZUKI","Vieja",100,16);
        System.out.println("  Resultado Revisión:"+rr.RECHAZADO);
        motoC = new motocicleta("HONDA","SEMINUEVA",150,18);
        System.out.println("  Resultado Revisión:"+rr.APROVADO);
        motoC = new motocicleta("KYMCO","SEMINUEVA",120,16);
        System.out.println("  Resultado Revisión:"+rr.APROVADO);
        motoC = new motocicleta("PIAGGIO","NUEVA",200,19);
        System.out.println("  Resultado Revisión:"+rr.RECHAZADO);
        motoC = new motocicleta("KAWASAKI","SEMINUEVA",160,16);
        System.out.println("  Resultado Revisión:"+rr.POSTERGADO);
        motoC = new motocicleta("KYMCO","NUEVA",120,16);
        System.out.println("  Resultado Revisión:"+rr.POSTERGADO);
        
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(centroRevisionVehicular.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        } else if (ct.getName().equals("Thread-2")) {
            //try {
            //inicio 2
            
             parqueadero parqueo = new parqueadero();
       
             Quito quito= new Quito();
            System.err.println("tercero");
          
     
        System.out.println("\n"+parqueo.tituloTotal);
        System.out.println("Capacidad: "+parqueo.capacidadMaxi+" Vehiculos Entrantes: "+parqueo.vehiculosRecibid+" Puestos Sobrantes: "+parqueo.puestosSobrantes);
        System.out.println("Porcentaje Livianos: "+parqueo.porcenLivian+" Porcentaje Autobuses: "+parqueo.porcenBuses+" Porcentaje Motos: "+parqueo.porcenMot);
        parqueo.analizarParqueadero();
        System.out.println();
       
        centroRevisionVehicular cen=new centroRevisionVehicular();
        
        //Uso de casting
        
        int NumClientes=0;
        
        if(NumClientes>10&NumClientes<300)
        {
            //Conversion directa
            long s=NumClientes;
            
        
        }
        short n2;
        if(NumClientes>300)
            //Conversion explicita
        {
            n2= (short)NumClientes;
        
        }
        
        System.err.println(cen.placas());
        //muestra Info de vehiculo acepatdo en parqueadero
        parqueadero parking=new parqueadero();
        parking.metodoAceptacionVehiculo();
              //Casting_de_Objetos
        //llamado al metodo de la Clase Interna
         Interna inte=new Interna();//instanciacion de la clase inner
        inte.imprimir();
        
        Interna2 interna2=new Interna2();//instaciacion de la clase method-local inner
        interna2.createLocalInnerClass();//llamado al metodo local que crea la clase interna
        Anonima anonim= new Anonima() {
            Propietario propi=new Propietario("172578901", "Julio Mora", 23, 2348);
            @Override
            void accionAnonima() {//sobrescribe el metodo de la clase anonima
                System.out.println(propi.toString()); 
            }
        };
        anonim.accionAnonima();//invocacion del metodo de la clase anonima en la clase main
        Anonima2 anon=new Anonima2() {
            Propietario propi=new Propietario("172578786", "Fernando Quezada", 24, 2349);
            @Override
            public void imprimir() {//sobrescribe el metodo de la clase anonima
                System.out.println(propi.toString()); 
            }
        };
        anon.imprimir();//invocacion del metodo de la clase anonima en la clase main
        Interna3.Anidada ani=new Interna3.Anidada();//instanciacion de la clases anidada de la Clase Interna3
        ani.multiplicacion();//llamado al metodo de la clase anidada
        Integer i1 = new Integer(42);
        Integer i2 = new Integer ("42");
        Boolean b = new Boolean ("false");
        Integer i3 = Integer.valueOf("101011", 2); //metodo valueOf Convierte el 101011 a 43 y le asigna el valor de 43al objeto Integer i3
        Integer i5 = new Integer(42);   // Crea un nuevo objeto wrapper
        byte b3 = i2.byteValue();        // Convierte el valor de i2 a un primitivo byte
        short s3= i2.shortValue();      // Otro de los métodos de Integer
        double d3 = i2.doubleValue();    // Otro de los métodos xxxValue de Integer
        double d4 = Double.parseDouble("3.14"); //  Convierte un String a primitivo
        System.out.println("d4 = " + d4);   //  El resultado será d4 = 3.14
        Double d5 = Double.valueOf("3.14"); //  Crea un objeto Double
        System.out.println(d5 instanceof Double);   //  El resultado es "true"



       // Exception try-catch 
       quito.Sucursales();
       
       //Exception propia
        String primeraletraplaca= "G";
 
        try {
            if (primeraletraplaca.equals("T")) {
                throw new ValidarPlacasPichincha("P");
            } else if (primeraletraplaca.equals("p")) {
                throw new ValidarPlacasPichincha("p");
            } else if (primeraletraplaca.equals("G")) {
                throw new ValidarPlacasPichincha("G");
            }
 
        } catch (ValidarPlacasPichincha ex) {
            System.out.println(ex.getMessage());
        }
       
        //Strings
        String confirmarPlaca = "P S D";
        String placaAuto = "PSD-648";
        
        String letra = ""+placaAuto.charAt(0);
        System.out.println(letra);
        
        if (letra.equals("P"))
        {
            System.out.println("La placa Corresponde a Pichincha");
        }
        
        String primeraletra = placaAuto.substring(0,1);
        
        if (primeraletra.contains("P"))
        {
            System.out.println("La placa Corresponde a Pichincha");
        }
        
        confirmarPlaca.startsWith("P");
        
        if (confirmarPlaca.startsWith("P"))
        {
            System.out.println("La placa Corresponde al DMQ");
        }
        
    
    String [] partes = placaAuto.split("-");
    
    String codigo = partes[1].substring(0, 1);
    
    String msg="";
    switch(codigo){
        
        case "6":
           msg = "Pico y Placa los Jueves";
           System.out.println(msg);
            break;
        case "0":
            msg = "Pico y Placa los Jueves";
            System.out.println(msg);
            break;
        default:
            break;         
    }
    
     StringBuilder sb = new StringBuilder(20);
     sb.append("final");
   
     System.out.println(sb.length()+" "+sb);
     
     StringBuffer sbuffer = new StringBuffer();
        long inicio = System.currentTimeMillis();
        for (int i=0; i<10000000; i++) {
            sbuffer.append("Placas");
        }
        long fin = System.currentTimeMillis();
        System.out.println("Tiempo del StringBuffer: " + (fin-inicio));  
        
        
        System.out.println("Ingresa al assert");       
        cen.pruebaAssert(parqueo.capacidadMaxima);
        cen.assertValidarVehRec(parqueo.vehiculosRecibidos);
        
         cen.assertValidarPuestosSob(parqueo.puestosSobrantes);
        
       cen.assertValidarPorcentajeLivia(parqueo.porcentajeLivianos);
            // } catch (InterruptedException ex) {
            //  Logger.getLogger(Balanceador.class.getName()).log(Level.SEVERE, null, ex);
            // }
        }
    }
        
        
        
        
      
            
    
    

    public String placas()
    {
       String dplaca="La placa correspondiente es: ";
        Random rnd=new Random();
       int numpla;
       numpla=(int)(rnd.nextDouble()*100+100)   ;
       char a,b,c;
       int aletorioa,aletoriob,aletorioc;
       aletoriob=(int)rnd.nextInt(50);
       aletorioc=(int)rnd.nextInt(50);
        
       aletorioa=(int)rnd.nextInt(50);
       System.err.println("a"+aletorioa+"b"+aletoriob+"c"+aletorioc);
       a=(char)(aletorioa+'0');
       b=(char)(aletoriob+'2');
       
       //c=(char)aletorioc;
       //c=(char)(aletorioc+'5');
       
       dplaca=a+b+"-"+numpla;
       return dplaca;
    }
    public static void Livianos(bloqueA bA)
    {
        bA.livianos();
    }
    
    public static void Rechazados(bloqueC bC)
    {
        bC.rechazados();
    }
    
    public static void Pesados(bloqueB bB)
    {
        bB.pesados();
    }
   
}
