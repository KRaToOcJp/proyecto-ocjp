/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ciudades;

import proyectoocjp.vehiculosTerrestres;

/**
 *
 * @author FERNANDO
 */
public class Quito extends vehiculosTerrestres{
    
    //variable final
    public final int codProvincia = 17;
  
    // llamar a la variable protected
    public String ciudad(){
        return Ciudad;
    }
    
    String sucursales[] = {"Guajalo", "Carapungo", "Guamaní", "San Isidro", "Florida", "Quitumbe"};
    
    //Exception con try-catch
    public void Sucursales(){
        
        try {
         System.out.println("Sucursales Quito :");
         System.out.println("" + sucursales[0]);
         System.out.println("" + sucursales[1]);
         System.out.println("" + sucursales[2]);
         System.out.println("" + sucursales[3]);
         System.out.println("" + sucursales[4]);
         System.out.println("" + sucursales[5]);
         System.out.println("" + sucursales[6]);
      }catch(ArrayIndexOutOfBoundsException e) {
         System.out.println("Exception: " + e);
      }
        finally{
            System.out.println("Son todas las sucursales de quito");
        }
      System.out.println("Fuera del arreglo");
      
      //uso de for
      for (int i=0; i<sucursales.length;i++){
          System.out.print("Sucursales: " + sucursales[i]+", "); 
      }
      System.out.println(""); 
   }
}
